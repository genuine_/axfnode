/**
 * User: glindor
 * Date: 4/20/13
 * Time: 3:46 PM
 */
var http = require('http'),
    path = require('path'),
    fs = require('fs'),
    util = require('util');

var extPath = './extensions';

var mimeTypes = {
    '.json': 'application/json'
};

http.createServer(_handleServer).listen(3000);


function _handleServer(req, res){
    url = "";
    root = "..";

    function fileExists(file){
        console.log("File is "+ file);
        if(file === false){
            res.writeHead(404);
            res.end("File you requested seems to be on vacation, permanently.");
            return;
        }
    }


    function serveFile(file){
        //read file and return json string
        var s = fs.readFileSync(file);
        console.log("file contents: " + s);

        //res.setHeader('Content-type',"text/json");
        //res.writeHead(200);
        res.end(s.toString());
        /*
        stream.on('error', function(error){
            res.writeHead(500);
            res.end("something happended while preparing to stream file");
            return;
        });

        res.setHeader('Content-type', 'text/json');
        res.writeHead(200);

        util.pump(stream, res,function(error){
            res.end("Error in pumping");
        });
        */

        return;
    }

    if(req.method !== 'GET'){
        res.writeHead(405);
        res.end("Function not yet supported.");

    }

    console.log("The requested url is " + req.url);

    if('.'+ req.url !== './'){
        filePath = extPath + req.url;
        console.log("filePath requested is " + filePath);
        fs.exists(filePath, fileExists);
        serveFile(filePath);
    }
    else{
        res.writeHead(405);
        res.end("something went terribly wrong!");
        return;
    }

}
